﻿# Jitesoft.Docker.Volume.SDK

## Observe

This package is in a very early stage, there are things missing and things might change drastically before a version 1.0.0 release. 
So be sure to check the changelogs when updating until the first stable release, after that the standard [SEMVER](https://semver.org/) rules will apply.

## Disclaimer

This package is not built by docker or anyone affiliated with docker, I.E., it's an unofficial SDK.  
Read the license.

## Package

The package contains a set of classes and interfaces which could be used to ease in making volume plugins for docker.  
The `Jitesoft.Docker.Volume.SDK.Interfaces.IApi` interface exposes all the methods that docker will use for its endpoints. It's intended to be implemented by a WebAPI which the docker client can send requests to.

The responses with expected properties can be found in the `Jitesoft.Docker.Volume.SDK.Responses` namespace and in the root namespace the API interface and structures used in the API can be found.

## Docker plugins

I have decided not to write a lengthy description of plugins here, but rather just link to the official [documentation](https://docs.docker.com/engine/extend/plugins_volume/) for volume plugins.

## Changelog

### 0.0.2

* Moved and renamed `IVolumeApi` class to `Interfaces/IApi`. 
* Exposed a `BaseException` class and a `VolumeNotFoundException` class which can be converted to error responses easily.
* Added json serialization with opt-out attributes to all responses and structures for quick conversion.
* Did some cleanup in the code.

### 0.0.1

Initial files.

## Issues and contributions

Feel free to add issues and submit pullrequests to the repository at gitlab.

[Issue tracker](https://gitlab.com/jitesoft/open-source/c-sharp/docker-plugins/SDK/volume/issues)  
[Repository](https://gitlab.com/jitesoft/open-source/c-sharp/docker-plugins/SDK/volume/)  
