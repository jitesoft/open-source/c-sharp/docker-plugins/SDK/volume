﻿#region File header.
// ///////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright 2018 - Jitesoft.
// File created 2018 06 05 (08:29)
// ///////////////////////////////////////////////////////////////////////////////////////////////////////
#endregion

using Newtonsoft.Json;

namespace Jitesoft.Docker.Volume.SDK
{
    /// <summary>
    ///     Representation of a single capability.
    /// </summary>
    [JsonObject(MemberSerialization.OptOut)]
    public class Capability
    {
        /// <summary>
        ///     Capability cope.
        /// </summary>
        public string Scope { get; set; }
    }
}