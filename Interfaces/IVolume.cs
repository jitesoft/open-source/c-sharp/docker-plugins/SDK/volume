﻿#region File header.
// ///////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright 2018 - Jitesoft.
// File created 2018 06 05 (11:58)
// ///////////////////////////////////////////////////////////////////////////////////////////////////////
#endregion

using System.Collections.Generic;

namespace Jitesoft.Docker.Volume.SDK.Interfaces
{
    /// <summary>
    ///     Volume contract.
    /// </summary>
    public interface IVolume
    {
        /// <summary>
        ///     Volume name.
        /// </summary>
        string Name { get; set; }

        /// <summary>
        ///     Volume mount point.
        /// </summary>
        string Mountpoint { get; set; }

        /// <summary>
        ///     Volume status.
        /// </summary>
        IDictionary<string, string> Status { get; set; }
    }
}