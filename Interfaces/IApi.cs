﻿#region File header.
// ///////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright 2018 - Jitesoft.
// File created 2018 06 05 (11:56)
// ///////////////////////////////////////////////////////////////////////////////////////////////////////
#endregion

using System.Collections.Generic;
using System.Threading.Tasks;
using Jitesoft.Docker.Volume.SDK.Exceptions;
using Jitesoft.Docker.Volume.SDK.Responses;

namespace Jitesoft.Docker.Volume.SDK.Interfaces
{
    /// <summary>
    ///     The base endpoints that are required - by the docker engine - to be implemented.
    /// </summary>
    public interface IApi
    {
        /// <summary>
        ///     Create volume.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="options"></param>
        /// <returns>EmptyResponse on success.</returns>
        Task<EmptyResponse> Create(string name, IDictionary<string, string> options);

        /// <summary>
        ///     GetResponse a single volume.
        /// </summary>
        /// <param name="name">Name of volume.</param>
        /// <returns>GetResponse on success.</returns>
        /// <exception cref="VolumeNotFoundException">On volume not found.</exception>
        Task<GetResponse> Get(string name);

        /// <summary>
        ///     GetResponse all managed volumes.
        /// </summary>
        /// <returns>ListResponse on success.</returns>
        Task<ListResponse> List();

        /// <summary>
        ///     Remove a given volume.
        /// </summary>
        /// <param name="name">Name of the volume to remove.</param>
        /// <returns>EmptyResponse on success.</returns>
        /// <exception cref="VolumeNotFoundException">On volume not found.</exception>
        Task<EmptyResponse> Remove(string name);

        /// <summary>
        ///     GetResponse the local path of a given volume.
        /// </summary>
        /// <param name="path">Name of the volume to get path for.</param>
        /// <returns>PathResponse on success.</returns>
        /// <exception cref="VolumeNotFoundException">On volume not found.</exception>
        Task<PathResponse> Path(string path);

        /// <summary>
        ///     MountResponse a given volume.
        /// </summary>
        /// <param name="name">Volume name.</param>
        /// <param name="id">Volume id.</param>
        /// <returns>MountResponse on success.</returns>
        /// <exception cref="VolumeNotFoundException">On volume not found.</exception>
        Task<MountResponse> Mount(string name, string id);

        /// <summary>
        ///     Unmount a given volume.
        /// </summary>
        /// <param name="name">Volume name.</param>
        /// <param name="id">Volume id.</param>
        /// <returns>EmptyResponse on success.</returns>
        /// <exception cref="VolumeNotFoundException">On volume not found.</exception>
        Task<EmptyResponse> Unmount(string name, string id);

        /// <summary>
        ///     GetResponse capabilities of the plugin.
        /// </summary>
        /// <returns>CapabilitiesResponse on success.</returns>
        Task<CapabilitiesResponse> Capabilities();
    }
}