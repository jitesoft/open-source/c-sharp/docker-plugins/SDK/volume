﻿#region File header.
// ///////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright 2018 - Jitesoft.
// File created 2018 06 05 (11:44)
// ///////////////////////////////////////////////////////////////////////////////////////////////////////
#endregion

using System;
using Jitesoft.Docker.Volume.SDK.Responses;

namespace Jitesoft.Docker.Volume.SDK.Exceptions
{
    /// <summary>
    ///     Baseclass for exceptions.
    ///     Provides a ToErrorResponse accessor for quick conversion.
    /// </summary>
    public abstract class BaseException : Exception
    {
        protected BaseException(string message) : base(message)
        {
        }

        protected BaseException(string message, Exception innerException) : base(message, innerException)
        {
        }

        /// <summary>
        ///     Convert the exception into an error response.
        /// </summary>
        /// <returns></returns>
        public ErrorResponse ToErrorResponse => new ErrorResponse
        {
            Err = Message
        };
    }
}