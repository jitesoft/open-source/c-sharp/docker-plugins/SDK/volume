﻿#region File header.
// ///////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright 2018 - Jitesoft.
// File created 2018 06 05 (10:04)
// ///////////////////////////////////////////////////////////////////////////////////////////////////////
#endregion

using System;

namespace Jitesoft.Docker.Volume.SDK.Exceptions
{
    /// <summary>
    ///     Exception thrown when a volume was not found.
    /// </summary>
    public class VolumeNotFoundException : BaseException
    {
        public VolumeNotFoundException(string message = "Failed to find volume.", string volumeIdentifier = null) : base(message)
        {
            Volume = volumeIdentifier;
        }

        public VolumeNotFoundException(string message, Exception innerException, string volumeIdentifier = null) : base(message, innerException)
        {
            Volume = volumeIdentifier;
        }

        /// <summary>
        ///     Identifier (name or id) of volume not found.
        /// </summary>
        public string Volume { get; }
    }
}