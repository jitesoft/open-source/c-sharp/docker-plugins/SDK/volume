﻿#region File header.
// ///////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright 2018 - Jitesoft.
// File created 2018 06 05 (08:29)
// ///////////////////////////////////////////////////////////////////////////////////////////////////////
#endregion

using System.Collections.Generic;
using Jitesoft.Docker.Volume.SDK.Interfaces;
using Newtonsoft.Json;

namespace Jitesoft.Docker.Volume.SDK
{
    /// <summary>
    ///     Structure representing a single volume.
    /// </summary>
    [JsonObject(MemberSerialization.OptOut)]
    public class Volume : IVolume
    {
        /// <summary>
        ///     Volume name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        ///     Volume mount point.
        /// </summary>
        public string Mountpoint { get; set; }

        /// <summary>
        ///     Volume status.
        /// </summary>
        public IDictionary<string, string> Status { get; set; }
    }
}