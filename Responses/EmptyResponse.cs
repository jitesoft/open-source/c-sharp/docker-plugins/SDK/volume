﻿#region File header.
// ///////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright 2018 - Jitesoft.
// File created 2018 06 05 (08:29)
// ///////////////////////////////////////////////////////////////////////////////////////////////////////
#endregion

using Newtonsoft.Json;

namespace Jitesoft.Docker.Volume.SDK.Responses
{
    /// <summary>
    ///     EmptyResponse structure.
    ///     Used to let the docker engine know that all is good.
    /// </summary>
    [JsonObject(MemberSerialization.OptOut)]
    public class EmptyResponse
    {
        /// <summary>
        ///     Always empty.
        ///     Used as a response to the docker engine showing that no error was present.
        /// </summary>
        public string Err { get; } = string.Empty;
    }
}