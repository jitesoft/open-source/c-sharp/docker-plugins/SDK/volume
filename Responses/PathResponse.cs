﻿#region File header.
// ///////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright 2018 - Jitesoft.
// File created 2018 06 05 (08:30)
// ///////////////////////////////////////////////////////////////////////////////////////////////////////
#endregion

using Newtonsoft.Json;

namespace Jitesoft.Docker.Volume.SDK.Responses
{
    /// <summary>
    ///     PathResponse structure.
    /// </summary
    [JsonObject(MemberSerialization.OptOut)]
    public class PathResponse
    {
        /// <summary>
        ///     Mountpoint of the volume.
        /// </summary>
        public string Mountpoint { get; set; }
    }
}