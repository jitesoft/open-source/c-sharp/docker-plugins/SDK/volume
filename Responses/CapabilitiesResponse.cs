﻿#region File header.
// ///////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright 2018 - Jitesoft.
// File created 2018 06 05 (08:29)
// ///////////////////////////////////////////////////////////////////////////////////////////////////////
#endregion

using System.Collections.Generic;
using Newtonsoft.Json;

namespace Jitesoft.Docker.Volume.SDK.Responses
{
    /// <summary>
    ///     CapabilitiesResponse structure.
    /// </summary>
    [JsonObject(MemberSerialization.OptOut)]
    public class CapabilitiesResponse
    {
        /// <summary>
        ///     ListResponse of capabilities.
        /// </summary>
        public IList<Capability> Capabilities { get; set; } = new List<Capability>();
    }
}