﻿#region File header.
// ///////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright 2018 - Jitesoft.
// File created 2018 06 05 (08:30)
// ///////////////////////////////////////////////////////////////////////////////////////////////////////
#endregion

using System.Collections.Generic;
using Newtonsoft.Json;

namespace Jitesoft.Docker.Volume.SDK.Responses
{
    /// <summary>
    ///     ListResponse structure.
    /// </summary>
    [JsonObject(MemberSerialization.OptOut)]
    public class ListResponse
    {
        /// <summary>
        ///     List of volumes the plugin handles.
        /// </summary>
        public IList<Volume> Volumes { get; set; } = new List<Volume>();
    }
}