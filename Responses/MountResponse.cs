﻿#region File header.
// ///////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright 2018 - Jitesoft.
// File created 2018 06 05 (08:30)
// ///////////////////////////////////////////////////////////////////////////////////////////////////////
#endregion

using Newtonsoft.Json;

namespace Jitesoft.Docker.Volume.SDK.Responses
{
    /// <summary>
    ///     MountResponse structure.
    /// </summary>
    [JsonObject(MemberSerialization.OptOut)]
    public class MountResponse
    {
        /// <summary>
        ///     Mountpoint of the volume.
        /// </summary>
        public string Mountpoint { get; set; }
    }
}